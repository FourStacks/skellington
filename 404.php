<?php get_header(); ?>

    <section class="Error404">

        <div class="u-container -narrow u-text-center">
            <h1>Error 404</h1>
            <p>Sorry, the page you were looking for couldn't be found.</p>
            <p>Try checking the address was typed correctly or use the navigation to move to a different part of the
                site.</p>
        </div>

    </section>


<?php get_footer(); ?>

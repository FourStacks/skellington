# Skellington - by FourStacks #

*This development-ready WP theme is loosely based on the brilliant Bones theme by Eddie Machado 
and has been heavily modified.  It uses a more partial-ised approach to asset management and several 
front end tools to handle task automation and dependency management:*

- Gulp handles front end task automation - Skellington leverages Laravel Elixir - http://laravel.com/docs/5.1/elixir -
 which acts as a wrapper around gulp and dramatically simplifies the creation of most regular gulp tasks (note that
Laravel Elixir can be used in any project that Gulp is used and is not tied to the Laravel framework 
in any way)
- NPM handles most 3rd party dependencies
- Any non NPM dependencies are managed using Bower


## Setup instructions ##


### Prerequisites ###

*These will only need to be installed once though they may need to be periodically updated*

1. Install node.js from http://nodejs.org/ (this will also install NPM)

2. Install bower at the command line: npm install -g bower (requires node.js - if you get an 
error try 'sudo install -g bower')

3. Install npm install -g gulp


### First time set up of theme ###

*Run these steps at the start of the project*

1. At command line, change directory to the theme root folder

2. Run: 'npm install' (grabs all required files for gulp tasks - creates a folder called node_modules)

3. Run: 'bower install' (grabs default bower dependencies - creates a folder called bower_components)

4. Go to the Gulpfile in the theme root and change the project URL to the development URL you are using


### During development ###

Now all the one-off steps have been run through it's time to start developing 
- here's where all the work above pays off!

1. At command line, change directory to the theme root folder

2. Run: 'gulp watch' (you can also run 'gulp' to perform a one-off run through of your tasks)

This does the following:

1. It spins up a browsersync server on http://localhost:3000/.  This handles live reloads when files are saved 
but also provides you with both a remote IP address that allows you to view your local development installation on 
any device that shares your LAN (e.g. if you're hooked up to the same wireless router).  Any changes made to the 
development copy will be instantly reflected on any device pointing to this IP.  It's friggin' cool.

2. Browserify will bundle up any npm dependicies and modules you've created

3. All your CSS and JS are watched for changes and files are concatenated.

4. CSS is passed to a PostCSS function to handle things like the Lost Grid framework

5. Any images added to or edited in assets/images will be optimised and the build version will be placed in 
/build/images


### For production ###

Simply run 'gulp --production' and gulp will minify all your assets.  Alternatively you can run 'gulp watch 
--production' to continue watching files.


### Partials ###

In the /partials directory you'll find a bunch of common design patterns.  More will be added over time.  Most of 
them have a matching CSS partial in assets/scss/components and some also have a JS partial in assets/js/modules.  
Also in the /partials directory you'll find a sub directory containing common snippets used with Advanced Custom Fields


### Authoring styles ###

1. CSS is written using a pretty traditional BEM syntax: e.g..BlockName\_\_element-name\-\-modifier\-name.  You're obviously free to use a different syntax if required.

2. JS modules are broken up into CommonJS modules which are required in the main app.js file using browserify.

3. Grids are handled using the excellent Lost grid framework by Cory Simmons https://github.com/corysimmons/lost

4. Vertical rhythm uses a standalone port of the Compass vertical rhythm module.  You can set up baseline sizes etc 
in assets/scss/global/_vars.  If you wish you can use the lh() function to maintain baseline consistency in your 
designs (see assets/scss/global/_vars)

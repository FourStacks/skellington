// Set up globals
require('./bootstrap');

// Import components
import ProjectItem from './components/ProjectItem.vue';

// import modules
import Scroll from './modules/Scrolling';

// Register vue components
Vue.component('project-item', ProjectItem);

// jQuery on ready
$(document).ready(() => {
	Scroll.init();
});

// On window resize
const onResize = () => {
	// Expensive stuff
};
window.addEventListener('resize', _.debounce(onResize, 150));

new Vue({
	el: '#projects'
});
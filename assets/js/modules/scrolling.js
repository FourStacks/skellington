export default {
    init () {
        $(".js-scroll-to-link a, .js-scroll-to").on("click", e => {
            // prevent default action and bubbling
            e.preventDefault();
            e.stopPropagation();
            // set target to anchor's "href" attribute
            let target = $(e.target).attr("href");
            // scroll to each target
            $(target).velocity("scroll", {
                duration: 500,
                offset: 0,
                easing: "ease-in-out"
            });
        });
    }
}
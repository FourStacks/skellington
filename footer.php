<footer class="bg-grey-lightest py-3" role="contentinfo">

    <div class="container mx-auto flex items-center">

        <p class="text-sm text-grey-darker mb-0">
            <a class="js-scroll-to no-underline text-red" href="#top">Top</a>
        </p>

        <p class="text-sm text-grey-darker ml-auto mb-0">
            &copy; Fred Company Ltd. Company no. XXXXXX
        </p>

    </div>

</footer>

<?php // all js scripts are loaded in library/theme-setup/enqueue.php ?>
<?php wp_footer(); ?>

</body>

</html>

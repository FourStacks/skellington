<?php

// Skellington file - primary theme functions and theme support setup
require_once( 'theme-setup/skellington.php' );

// Custom post type setup
// require_once('theme-setup/custom-post-type.php');

// Admin customisation functionality
require_once( 'theme-setup/admin.php' );

// Enqueue scripts and styles
require_once('theme-setup/enqueue.php');

// Image sizes and additional theme support for images
require_once('theme-setup/image-sizes.php');

// Menu registration
require_once('theme-setup/menu-registration.php');

// IE scripts
require_once('theme-setup/ie-shims.php');

// Utilities
require_once('theme-setup/utilities.php');

// Gravity forms
// require_once('theme-setup/gravity-forms.php');

// Sidebar registration
require_once('theme-setup/widget-areas.php');

// Icons
require_once('theme-setup/icons.php');

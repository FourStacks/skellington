<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php // Page content here ?>
	<section style="padding: 72px 0">
		<div class="container">
			<h2>Hey, welcome to Skellington</h2>
			<p>This is the index.php template - you'll probably want to get rid of the section that I live in and
				create something nicer.  Then again you'll probably not be using index.php at all so you can pretty
				much ignore this entirely...</p>
		</div>
	</section>

<?php endwhile; endif; ?>
<?php get_footer(); ?>

<?php
/*
Template Name: Home Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
        get_template_part('partials/sections/hero');
        skellington_primary_nav();
        get_template_part('partials/sections/about');
        get_template_part('partials/sections/partners');
        get_template_part('partials/sections/projects');
        get_template_part('partials/sections/why-we-play');
        get_template_part('partials/sections/blog');
        get_template_part('partials/sections/meet-fred');
        get_template_part('partials/sections/play-with-fred');
    ?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>

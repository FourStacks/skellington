<?php

$location = get_field('location');

if( !empty($location) ):
    ?>
    <div class="AcfMap">
        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
    </div>
<?php endif; ?>


<?php if( have_rows('locations') ): ?>
    <div class="AcfMap">
        <?php while ( have_rows('locations') ) : the_row();

            $location = get_sub_field('location');

            ?>
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                <h4><?php the_sub_field('title'); ?></h4>
                <p class="address"><?php echo $location['address']; ?></p>
                <p><?php the_sub_field('description'); ?></p>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>

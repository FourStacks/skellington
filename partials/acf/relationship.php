<?php

$posts = get_field('relationship_field_name');

if( $posts ): ?>
    <?php foreach( $posts as $post): setup_postdata($post); ?>
        
        <?php the_permalink(); ?>

    <?php endforeach; ?>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
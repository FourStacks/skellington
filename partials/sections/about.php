<section id="about-fred" class="bg-grey-lightest py-16 text-center px-4 md:px-0">
    <div class="max-w-md mx-auto">
        <h2 class="font-serif text-red text-3xl mb-6 lowercase">
            <?= get_field('about_title');?>
        </h2>
        <h3 class="font-light text-2xl mb-6">
            <?= get_field('about_subtitle');?>
        </h3>
        <div class="text-lg">
            <?= get_field('about_text');?>
        </div>
    </div>
</section>
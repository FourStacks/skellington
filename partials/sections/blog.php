<section class="py-16 text-center px-4 bg-pattern">
    <div class="max-w-md mx-auto ">
        <h2 class="font-serif text-white text-3xl mb-6 lowercase">
            <?= get_field('blog_title');?>
        </h2>

        <div class="text-lg mb-12 text-white">
            <?= get_field('blog_text');?>
        </div>
    </div>

    <div class="container mx-auto">
        <?= do_shortcode( '[display_medium_posts handle="@FEhrsam"]' ); ?>
    </div>

</section>
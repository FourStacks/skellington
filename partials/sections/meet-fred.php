<section id="meet-fred" class="py-16 text-center px-4 md:px-0">

    <div class="max-w-md mx-auto mb-12">
        <h2 class="font-serif text-red text-3xl mb-6 lowercase">
            <?= get_field('team_title');?>
        </h2>
        <div class="text-lg">
            <?= get_field('team_text');?>
        </div>
    </div>

    <?php if( have_rows('team') ): ?>
        <ul class="list-reset container mx-auto lg:flex lg:flex-wrap">
        <?php while ( have_rows('team') ) : the_row();?>
            <li class="mb-12 sm:flex lg:w-1/2 px-3">
                <?php $image = get_sub_field('image'); ?>
                <figure
                    class="h-24 w-24 rounded-full bg-cover mx-auto mb-4 flex-no-shrink sm:mr-6 sm:h-32 sm:w-32"
                    style="background-image: url('<?= $image['url'];?>')"
                ></figure>
                <div class="sm:text-left">
                    <h4 class="font-bold mb-4"><?php the_sub_field('name'); ?></h4>
                    <p><?php the_sub_field('bio'); ?></p>
                </div>
            </li>
        <?php endwhile;?>
        </ul>
    <?php endif; ?>
</section>
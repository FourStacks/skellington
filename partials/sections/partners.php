<section class="py-16 text-center px-4 md:px-3">
    <div class="container mx-auto">
        <div class="max-w-md mx-auto mb-6">
            <h2 class="font-serif text-red text-3xl mb-6 lowercase">
                <?= get_field('partners_title');?>
            </h2>

            <div class="text-lg">
                <?= get_field('partners_text');?>
            </div>
        </div>
        <ul class="list-reset flex flex-wrap justify-center -mx-3">
            <?php if( have_rows('partner_testimonials') ): ?>

                <?php while ( have_rows('partner_testimonials') ) : the_row();?>
                    <?php $image = get_sub_field('logo'); ?>
                    <li class="w-1/2 px-3 mb-6 md:w-1/3">
                        <div class="border-2 border-grey-lightest w-full h-32 p-4 flex items-center justify-center">
                            <img class="max-w-full max-h-full" src="<?= $image['url'];?>" alt="<?= $image['alt'];?>">
                        </div>
                    </li>
                <?php endwhile;?>

            <?php endif; ?>
        </ul>
    </div>
</section>
<section class="py-16 text-center px-4 bg-pattern">
    <div class="max-w-md mx-auto ">
        <h2 class="font-serif text-white text-3xl mb-6 lowercase">
            <?= get_field('contact_title');?>
        </h2>

        <div class="text-lg mb-12 text-white">
            <?= get_field('contact_text');?>
        </div>

        <ul class="list-reset md:flex items-start">
            <li class="mb-6 md:w-1/3 md:px-3">
                <a class="flex items-center justify-center rounded-full border-white border-2 h-16 w-16 mx-auto mb-3" href="https://twitter.com/TheFredCompany">
                    <svg class="text-white fill-current" version="1.1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <title>twitter</title>
                        <path d="M23.6 2.2c-0.3-0.2-0.8-0.2-1.1 0-0.7 0.5-1.5 0.9-2.3 1.2-2-1.8-5.1-1.9-7.2-0.1-1.3 1.1-2 2.6-2 4.2-2.9-0.2-5.5-1.7-7.2-4.1-0.2-0.3-0.5-0.4-0.9-0.4s-0.7 0.3-0.8 0.6c-0.1 0.1-1.1 2.5-1 5.4 0.1 2.5 1.1 5.7 4.8 8-1.5 0.7-3.2 1-4.9 1-0.4 0-0.9 0.3-1 0.7s0.1 0.9 0.5 1.1c2.5 1.4 5.2 2.1 7.7 2.1s4.9-0.6 7.1-1.9c4.3-2.4 6.7-7 6.7-12.5 0-0.2 0-0.3 0-0.5 1-1.1 1.7-2.4 2-3.8 0.1-0.4-0.1-0.8-0.4-1zM20.2 6c-0.2 0.2-0.3 0.6-0.3 0.9 0.1 0.2 0.1 0.4 0.1 0.6 0 4.8-2.1 8.7-5.7 10.8-2.8 1.6-6.1 2-9.4 1.2 1.3-0.4 2.5-0.9 3.6-1.7 0.4-0.2 0.5-0.5 0.5-0.9s-0.3-0.7-0.6-0.8c-5.8-2.6-5.6-7.5-5-10 2.2 2.2 5.3 3.5 8.6 3.4 0.5 0 1-0.5 1-1v-1c0-1 0.4-2 1.2-2.7 1.4-1.3 3.7-1.1 4.9 0.3 0.3 0.3 0.7 0.4 1 0.3 0.2-0.1 0.5-0.1 0.7-0.2-0.2 0.3-0.4 0.5-0.6 0.8z"></path>
                    </svg>
                </a>
                <a class="text-white text-center no-underline" href="https://twitter.com/TheFredCompany">
                    <?php the_field('twitter_text');?>
                </a>
            </li>
            <li class="mb-6 md:w-1/3 md:px-3">
                <a class="flex items-center justify-center rounded-full border-white border-2 h-16 w-16 mx-auto mb-3" href="https://twitter.com/TheFredCompany">
                    <svg class="text-white fill-current" version="1.1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <title>facebook2</title>
                        <path d="M18 7c0.6 0 1-0.4 1-1v-4c0-0.6-0.4-1-1-1h-3c-3.3 0-6 2.7-6 6v2h-2c-0.6 0-1 0.4-1 1v4c0 0.6 0.4 1 1 1h2v7c0 0.6 0.4 1 1 1h4c0.6 0 1-0.4 1-1v-7h2c0.5 0 0.9-0.3 1-0.8l1-4c0.1-0.3 0-0.6-0.2-0.9s-0.5-0.3-0.8-0.3h-3v-2h3zM14 11h2.7l-0.5 2h-2.2c-0.6 0-1 0.4-1 1v7h-2v-7c0-0.6-0.4-1-1-1h-2v-2h2c0.6 0 1-0.4 1-1v-3c0-2.2 1.8-4 4-4h2v2h-2c-1.1 0-2 0.9-2 2v3c0 0.6 0.4 1 1 1z"></path>
                    </svg>
                </a>
                <a class="text-white text-center no-underline" href="https://twitter.com/TheFredCompany">
                    <?php the_field('facebook_text');?>
                </a>
            </li>
            <li class=" md:w-1/3 md:px-3">
                <a class="flex items-center justify-center rounded-full border-white border-2 h-16 w-16 mx-auto mb-3" href="https://twitter.com/TheFredCompany">
                    <svg class="text-white fill-current" version="1.1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <title>mail</title>
                        <path d="M20 3h-16c-1.7 0-3 1.3-3 3v12c0 1.7 1.3 3 3 3h16c1.7 0 3-1.3 3-3v-12c0-1.7-1.3-3-3-3zM4 5h16c0.4 0 0.7 0.2 0.9 0.6l-8.9 6.2-8.9-6.2c0.2-0.4 0.5-0.6 0.9-0.6zM20 19h-16c-0.6 0-1-0.4-1-1v-10.1l8.4 5.9c0.2 0.1 0.4 0.2 0.6 0.2s0.4-0.1 0.6-0.2l8.4-5.9v10.1c0 0.6-0.4 1-1 1z"></path>
                    </svg>
                </a>
                <a class="text-white text-center no-underline" href="mailto:rosie@thefred.company">
                    <?php the_field('email_text');?>
                </a>
            </li>
        </ul>
    </div>


</section>
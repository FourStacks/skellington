<section id="what-we-do" class="py-16 text-center mx-auto px-4 bg-grey-lightest">
    <h2 class="font-serif text-red text-3xl mb-12 lowercase">
        <?= get_field('projects_title');?>
    </h2>
    <div id="projects">
    <?php if( have_rows('projects') ): $i = 1; ?>

        <?php while ( have_rows('projects') ) : the_row();?>

            <?php
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $summary = get_sub_field('summary');
                $full_text = get_sub_field('full_text');
                $button_text = get_sub_field('button_text');
                $swap_image = $i % 2 == 0;
            ?>
            <project-item
                image-url="<?= $image['sizes']['app-image-large'];?>"
                button-default-text="<?= $button_text;?>"
                :swap-image="<?= json_encode($swap_image);?>"
            >
                <div slot="summary" class="mb-8">
                    <h3 class="text-lg font-light mb-4">
                        <?= $title;?>
                    </h3>
                    <p class="text-base"><?= $summary;?></p>
                </div>
                <div slot="content" class="mb-8">
                    <h3 class="text-lg font-light mb-4">
                        <?= $title;?>
                    </h3>
                    <?= $full_text;?>
                </div>
            </project-item>

        <?php $i++; endwhile;?>
    <?php endif; ?>
    </div>
</section>
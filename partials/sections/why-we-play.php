<section id="why-we-play" class="py-16 text-center px-4 md:px-0">
    <div class="max-w-md mx-auto ">
        <h2 class="font-serif text-red text-3xl mb-6 lowercase">
            <?= get_field('why_play_title');?>
        </h2>

        <h3 class="font-light text-2xl mb-6">
            <?= get_field('why_play_subtitle');?>
        </h3>

        <div class="text-lg mb-12">
            <?= get_field('why_play_text');?>
        </div>
    </div>

    <div class="container mx-auto max-w-lg">
        <?php if( have_rows('approaches') ): $i = 1; ?>
            <ul class="list-reset md:flex md:justify-center md:flex-wrap">
            <?php while ( have_rows('approaches') ) : the_row();?>
                <?php
                    $widthClasses = ($i <= 3)
                        ? 'md:w-1/3'
                        : 'md:w-1/2';
                    $i ++;
                ?>
                <li class="mb-8 md:px-6 <?= $widthClasses;?>">
                    <div class="h-16 mb-4">
                        <?= getIconFromSlug(get_sub_field('icon')); ?>
                    </div>
                    <h4 class="font-thin text-base leading-normal">
                        <?php the_sub_field('wrong_approach'); ?>
                    </h4>
                    <p class="font-bold text-base leading-normal">
                        <?php the_sub_field('correct_approach'); ?>
                    </p>
                </li>
            <?php endwhile;?>
            </ul>
        <?php endif; ?>
    </div>

</section>
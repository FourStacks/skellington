<?php

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'skellington_flush_rewrite_rules' );

// Flush your rewrite rules
function skellington_flush_rewrite_rules() {
	flush_rewrite_rules();
}

function custom_post_types() {

	require_once('post-types/post-type.php');
	
}

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_types');

require_once('taxonomies/custom-taxonomy.php');

<?php

/*********************
 * SCRIPTS & ENQUEUEING
 *********************/

function skellington_scripts_and_styles()
{
    // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
    global $wp_styles;

    global $is_IE; // check if user is using IE

    if (!is_admin())
    {

        // modernizr - goes in head
        //wp_register_script('skellington-modernizr', get_stylesheet_directory_uri() . '/build/js/standalone/modernizr.min.js', array(), '', false);

        // register main stylesheet
        wp_register_style('skellington-stylesheet', get_stylesheet_directory_uri() . '/public/css/app.css', array(), '','all');

        //adding scripts file in the footer
        wp_register_script('skellington-js', get_stylesheet_directory_uri() . '/public/js/app.js', array('jquery'), '', true);

        // enqueue styles and scripts
        wp_enqueue_script('skellington-modernizr');
        wp_enqueue_style('skellington-stylesheet');

        // add conditional wrapper around ie stylesheet
        $wp_styles->add_data('skellington-ie-only', 'conditional', 'lt IE 9');

        // enqueue scripts
        wp_enqueue_script('jquery');
        wp_enqueue_script('skellington-js');

    }
}

?>
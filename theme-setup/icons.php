<?php

function getIconFromSlug($slug){
    switch($slug){

        case 'announcement':
            return '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="heroicon-announcement heroicon heroicons-lg max-h-full">
    <path class="heroicon-announcement-bowl heroicon-component-fill" d="M88 9.98A82.6 82.6 0 0 1 46 26.6v28.78a82.6 82.6 0 0 1 42 16.63V9.98z"></path>
    <path class="heroicon-announcement-front heroicon-component-accent heroicon-component-fill" d="M94 36V4a2 2 0 0 0-2-2h-2v78h2a2 2 0 0 0 2-2V46h2a2 2 0 0 0 2-2v-6a2 2 0 0 0-2-2h-2z"></path>
    <polygon class="heroicon-announcement-middle heroicon-component-accent heroicon-component-fill" points="11 57 11 25 44 25 44 57 42 57 42 53 18 53 18.4 57"></polygon>
    <path class="heroicon-announcement-handle heroicon-component-accent heroicon-component-fill" d="M37.55 71h2.54a2 2 0 0 0 1.86-2.74l-1.77-7.43-.18.03V55H20.21l.98 9.84c.14 1.33.63 2.31 1.37 2.9a2.9 2.9 0 0 0 1.64.6l.13.01c.25.01.52 0 .8-.06l6.05 27.4A3.1 3.1 0 0 0 34 98h4.98c1.01 0 1.65-.72 1.53-1.72L37.55 71z"></path>
    <path class="heroicon-announcement-back heroicon-component-fill" d="M3 30H1v22h2v1a3 3 0 0 0 3 3h5V26H6a3 3 0 0 0-3 3v1z"></path>
    <path class="heroicon-shadows" d="M46 55.4v-14a82.6 82.6 0 0 1 42 16.62v14A82.6 82.6 0 0 0 46 55.4zM26.99 74.72l-1.12-5.58c.33-.1.66-.22 1-.37l9.66-4.4.71 5.7L27 74.72zM10 31v2H8a1 1 0 1 1 0-2h2zm0 6v2H8a1 1 0 1 1 0-2h2zm0 6v2H8a1 1 0 1 1 0-2h2zm0 6v2H8a1 1 0 1 1 0-2h2zm30 8v4H20v-4h-8V41h32v16h-4zm49-16h6v37a3 3 0 0 1-3 3h-3V41z"></path>
    <path class="heroicon-outline" fill-rule="nonzero" d="M88 0h4a4 4 0 0 1 4 4v30a4 4 0 0 1 4 4v6a4 4 0 0 1-4 4v30a4 4 0 0 1-4 4h-4v-7.45A79.62 79.62 0 0 0 46 57.4V59h-4v4l1.8 4.51A4 4 0 0 1 40.1 73h-.48l2.88 23.04A3.44 3.44 0 0 1 39 100h-5a5.09 5.09 0 0 1-4.79-3.93l-5.14-25.73c-2.58-.16-4.55-2.13-4.87-5.3L18.6 59H10v-2H6a4 4 0 0 1-4-4H0V29h2a4 4 0 0 1 4-4h4v-2h36v1.6A79.62 79.62 0 0 0 88 7.46V0zm2 2v78h2a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2h-2zm6 44a2 2 0 0 0 2-2v-6a2 2 0 0 0-2-2v10zM10 27H6a2 2 0 0 0-2 2v24c0 1.1.9 2 2 2h4v-3H8a2 2 0 1 1 0-4h2v-2H8a2 2 0 1 1 0-4h2v-2H8a2 2 0 1 1 0-4h2v-2H8a2 2 0 1 1 0-4h2v-3zm0 4H8a1 1 0 1 0 0 2h2v-2zM88 9.98A82.6 82.6 0 0 1 46 26.6v28.78a82.6 82.6 0 0 1 42 16.63V9.98zm-38 20.7v-1.02a84.98 84.98 0 0 0 34-12.58v1.19a85.96 85.96 0 0 1-34 12.4zM10 37H8a1 1 0 1 0 0 2h2v-2zm0 6H8a1 1 0 1 0 0 2h2v-2zm0 6H8a1 1 0 1 0 0 2h2v-2zm-8 2h1V31H2v20zm10-26v32h2V25h-2zm3 32h3.4l-.4-4h24v4h2V25H15v32zm24.38 14h.71a2 2 0 0 0 1.86-2.74l-1.77-4.43-1.61.73.8 6.44zM24.2 68.35h.13c.64.02 1.36-.13 2.12-.48l9.95-4.52L40 61.7V55H20.21l.98 9.84c.14 1.33.63 2.31 1.37 2.9a2.9 2.9 0 0 0 1.64.6zm6.98 27.33A3.1 3.1 0 0 0 34 98h4.98c1.01 0 1.65-.72 1.53-1.72l-3.99-31.9-9.66 4.4c-.34.15-.67.28-1 .37l5.3 26.53zM23 59a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm15-1a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"></path>
</svg>';
            break;

        case 'chart':
            return '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="heroicon-chart heroicon heroicons-lg max-h-full">
    <path class="heroicon-chart-stand heroicon-component-accent heroicon-component-fill" d="M51 2v2h-2V2h2zm18.6 64H22v-2h56v2h-8.4zm-1.57 2l7.5 30h-2.06l-7.5-30h2.06zM49 68h2v20h-2V68zm-17.03 0h2.06l-7.5 30h-2.06l7.5-30z"></path>
    <polygon class="heroicon-chart-poster heroicon-component-fill" points="79 65 99 65 99 5 1 5 1 65 21 65 21 63 79 63"></polygon>
    <polygon class="heroicon-chart-stripe heroicon-component-accent heroicon-component-fill" points="2 11 98 11 98 12 2 12"></polygon>
    <path class="heroicon-shadows" d="M36.1 68l-1.26 5h-6.18l1.25-5h6.18zm34 0l1.24 5h-6.18l-1.25-5h6.18zM53 68v8h-6v-8h6z"></path>
    <path class="heroicon-outline" fill-rule="nonzero" d="M47 4V0h6v4h47v62H80v2h-9.9l7.5 30 .5 2H71.9l-8-32H53v22h-6V68H36.1l-8 32H21.9l.5-2 7.5-30H20v-2H0V4h47zm4-2h-2v2h2V2zm18.6 64H78v-2H22v2h47.59zm-1.57 2h-2.06l7.5 30h2.06l-7.5-30zM49 68v20h2V68h-2zm-17.03 0l-7.5 30h2.06l7.5-30h-2.06zM2 6v4h96V6H2zm0 5v1h96v-1H2zm96 53V13H2v51h18v-2h60v2h18zM37 45.47l-9.32 5.59-9.84-1.97L9.38 58H93v1H7V21h1v35.55l9.16-9.64 10.16 2.03 9.95-5.97 9.89 1 10.22-9.2 10.16 6.09 9.3-4.65L86.9 23.15l4.8-1.6a1 1 0 0 1 .63 1.9l-4.21 1.4-9.96 12.94-.15.08V41h-1v-2.63l-9.54 4.77L58 37.47V41h-1v-3.2l-9 8.1V49h-1v-3.05l-9-.9V49h-1v-3.53zM88 29v4h-1v-4h1zm-1 8h1v4h-1v-4zm0 8h1v4h-1v-4zm0 8h1v4h-1v-4zm-29-8v4h-1v-4h1zm0 8v4h-1v-4h1zm-21 0h1v4h-1v-4zm-19 0v4h-1v-4h1zm9 0h1v4h-1v-4zm20 0h1v4h-1v-4zm21-8v4h-1v-4h1zm-1 8h1v4h-1v-4zm11-8v4h-1v-4h1zm0 8v4h-1v-4h1zM66 17v2H34v-2h32z"></path>
</svg>
';
            break;

        case 'stethoscope':
            return '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="heroicon-stethoscope heroicon heroicons-lg max-h-full">
    <path class="heroicon-stethoscope-eartips heroicon-component-accent heroicon-component-fill" d="M40 1h3.99A4 4 0 0 1 48 5v25h-6V7h-2v1h-4a4 4 0 0 1 0-8h4v1zM8 1H4.01A4 4 0 0 0 0 5v25h6V7h2v1h4a4 4 0 0 0 0-8H8v1z"></path>
    <circle class="heroicon-stethoscope-chestpiece heroicon-component-accent heroicon-component-fill" cx="92" cy="50" r="8"></circle>
    <path class="heroicon-stethoscope-tube heroicon-component-fill" d="M2 38v-8h2v8a20 20 0 1 0 40 0v-8h2v8a22 22 0 0 1-19.25 21.83l-1.75.22V80a16 16 0 0 0 32 0V20a18 18 0 0 1 36 0v22.06a8.09 8.09 0 0 0-2 0V20a16 16 0 0 0-32 0v60a18 18 0 0 1-36 0V60.05l-1.75-.22A22 22 0 0 1 2 38z"></path>
    <path class="heroicon-outline" fill-rule="nonzero" d="M40 7v1h-4a4 4 0 1 1 0-8h4v1h4a4 4 0 0 1 4 4v33a24 24 0 0 1-21 23.81V80a14 14 0 1 0 28 0V20a20 20 0 1 1 40 0v22.58a8 8 0 1 1-6 0V20a14 14 0 1 0-28 0v60a20 20 0 1 1-40 0V61.81A24 24 0 0 1 0 38V5a4 4 0 0 1 4-4h4V0h4a4 4 0 1 1 0 8H8V7H6v31a18 18 0 0 0 36 0V7h-2zM4 5h4V3H4a2 2 0 0 0-2 2v24h2V5zM2 38a22 22 0 0 0 19.25 21.83l1.75.22V80a18 18 0 0 0 36 0V20a16 16 0 0 1 32 0v22.06a8.09 8.09 0 0 1 2 0V20a18 18 0 0 0-36 0v60a16 16 0 0 1-32 0V60.05l1.75-.22A22 22 0 0 0 46 38v-8h-2v8a20 20 0 1 1-40 0v-8H2v8zm44-9V5a2 2 0 0 0-2-2h-4v2h4v24h2zM38 6V2h-2a2 2 0 1 0 0 4h2zM12 6a2 2 0 1 0 0-4h-2v4h2zm86 44a6 6 0 1 0-12 0 6 6 0 0 0 12 0zm-2 0a4 4 0 1 1-8 0 4 4 0 0 1 8 0zm-4 3a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm1-3a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"></path>
</svg>';
            break;

        case 'lightning':
            return '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="heroicon-lightning heroicon heroicons-lg max-h-full">
    <polygon class="heroicon-lightning-front heroicon-component-fill" points="71.719 42 62 42 59.046 42 53.8 42 52.323 42 52.871 40.629 68.323 2 31.64 2 20.44 58 42 58 44.44 58 43.961 60.392 39.613 82.133"></polygon>
    <path class="heroicon-lightning-side heroicon-component-accent heroicon-component-fill" d="M59.45 41H53.8l.4-1L69.4 2h5.65l-14.9 37.26-.7 1.74zM39.19 84.27L73 42h4.84L37.57 92.33l1.62-8.06z"></path>
    <path class="heroicon-outline" fill-rule="nonzero" d="M42 60H18l.2-1 .2-1L29.8 1l.2-1h48l-.8 2-14.4 36-.8 2H82l-1.6 2-43.68 54.6L34 100l.85-4.27L41.6 62l.4-2zm17.45-19l.7-1.74L75.04 2H69.4L54.2 40l-.4 1h5.65zm12.27 1H52.32l.55-1.37L68.32 2H31.64l-11.2 56h24l-.48 2.4-4.35 21.73L71.71 42zM39.19 84.27l-1.62 8.06L77.84 42H73L39.19 84.27zM35 6h12l-.2 1H35.82l-2.8 14H32l3-15zm-4 20h1.02l-2 10H29l2-10z"></path>
</svg>';
            break;

        case 'knife':
            return '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="heroicon-multitask-knife heroicon heroicons-lg max-h-full">
    <rect class="heroicon-multitask-knife-housing heroicon-component-accent heroicon-component-fill" width="98" height="28" x="1" y="62" rx="14"></rect>
    <path class="heroicon-multitask-knife-knives heroicon-component-fill" d="M85.14 61.17l-.14-.14V55.4l4-2v1.53l-.9.44-1.1.56V61.3a15.1 15.1 0 0 0-1.86-.13zm.75-38.21l1.11-.55V16l2-2v8.94l-.9.44-3.1 1.56V23.4l.9-.45zm-.65 28.1l1.52-.77 2 1-1.52.76-2-1zm0-24l1.52-.77 2 1-1.52.76-2-1zm0 8l1.52-.77 2 1-1.52.76-2-1zM89 30.93l-4 2V31.4l4-2v1.53zm-4 8.47l4-2v1.53l-4 2V39.4zm2.24 4.64l-2-1 1.52-.76 2 1-1.52.76zM89 46.94l-4 2V47.4l4-2v1.53zM21 61h-6c-2.13 0-4.16.45-6 1.25V21h2.65l6.4 16H11v14a4 4 0 0 0 8 0v-1.17l2 2V61zm38.69 0L41.45 42.77a27.98 27.98 0 0 1-7.6-25.54l1.42.96a28.46 28.46 0 0 0 7.95 24.93L61.1 61h-1.41zm2.82 0L43.93 42.41a27.46 27.46 0 0 1-7.74-23.61l11.77 7.84L82.3 61h-19.8z"></path>
    <path class="heroicon-multitask-knife-front-details heroicon-component-fill" d="M84 74h-4v4h4v4h4v-4h4v-4h-4v-4h-4v4zM0 73h12v6H0v-6z"></path>
    <path class="heroicon-outline" fill-rule="nonzero" d="M85 15l4-4 2-2v13.76a2 2 0 0 1-1.1 1.8L89 25l2 1v6l-2 1 2 1v6l-2 1 2 1v6l-2 1 2 1v6l-2 1v4.54A15 15 0 0 1 85 91H15A15 15 0 0 1 .3 79H0v-6h.3c.83-4.07 3.3-7.54 6.7-9.7V19h6l7.2 18 .8 2h-8v12a2 2 0 1 0 4 0v-6l2 2 4 4v10h33.86L40.04 44.18a29.98 29.98 0 0 1-7.43-30.17l1.74 1.16 1.36.9.89.6 12.63 8.42L83 58.86V54l2-1-2-1v-6l2-1-2-1v-6l2-1-2-1v-6l2-1-2-1v-6l2-1v-6zM15 89h70a13 13 0 0 0 0-26H15A13 13 0 0 0 2.35 73H12v6H2.35A13 13 0 0 0 15 89zm44.69-28h1.41L43.22 43.12a28.46 28.46 0 0 1-7.95-24.93l-1.43-.96a27.98 27.98 0 0 0 7.61 25.54L59.7 61zm3.13 0l9.04-9.04.7.7L64.23 61h18.08L47.96 26.64 36.19 18.8a27.46 27.46 0 0 0 7.74 23.61L62.5 61h.3zm22.32 0c.63 0 1.25.05 1.86.13v-5.37l1.1-.55.9-.45v-1.52l-4 2v5.62l.14.14zM21 61v-9.17l-2-2V51a4 4 0 0 1-8 0V37h7.05l-6.4-16H9v41.25c1.84-.8 3.87-1.25 6-1.25h6zm64.9-38.21l-.9.45v1.52l3.1-1.55.9-.45v-8.93l-2 2v6.41l-1.1.55zm-.66 28.1l2 1 1.52-.77-2-1-1.52.76zm0-24l2 1 1.52-.77-2-1-1.52.76zm0 8l2 1 1.52-.77-2-1-1.52.76zM89 30.75v-1.52l-4 2v1.52l4-2zm-4 8.48v1.52l4-2v-1.52l-4 2zm2.24 4.64l1.52-.76-2-1-1.52.76 2 1zM89 46.76v-1.52l-4 2v1.52l4-2zM11 74H1v4h10v-4zm73 0v-4h4v4h4v4h-4v4h-4v-4h-4v-4h4zm1 0v1h-4v2h4v4h2v-4h4v-2h-4v-4h-2v3zM18 64h24v1H18v-1zm0 23h64v1H18v-1zm28-23h4v1h-4v-1zm16 0v1h-8v-1h8zm-4.28-24.77l-.71.7-8.49-8.48.71-.7 8.49 8.48z"></path>
</svg>';
            break;
    }
}
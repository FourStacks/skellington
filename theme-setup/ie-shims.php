<?php
// add ie conditional html5 shim to header
function add_ie_shims () {
	global $is_IE;
	if ($is_IE) {
   	echo '<!--[if lt IE 9]>';
    echo '<script src="' . get_stylesheet_directory_uri() . '/library/js/build/libs/html5shiv.min.js' . '"></script>';
    echo '<script src="' . get_stylesheet_directory_uri() . '/library/js/build/libs/selectivizr.min.js' . '"></script>';
    echo '<![endif]-->';
  }
}

add_action('wp_head', 'add_ie_shims');
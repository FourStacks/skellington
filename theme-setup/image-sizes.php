<?php

// Thumbnail sizes
add_image_size( 'app-image-large', 600, 400, true );
add_image_size( 'app-image-medium', 300, 200, true );

add_filter( 'image_size_names_choose', 'skellington_custom_image_sizes' );

function skellington_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'app-image-large' => __('Large - 600px by 400px'),
        'app-image-medium' => __('Medium - 300px by 200px'),
    ));
}
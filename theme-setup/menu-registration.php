<?php

/*********************
 * MENUS & NAVIGATION
 *********************/

// the main menu
function skellington_primary_nav()
{

    wp_nav_menu(array(
        'container' => false,                           // remove nav container
        'menu' => 'Primary Navigation',                 // nav name
        'menu_class' => 'masthead-nav',   // adding custom nav class
        'theme_location' => 'primary-nav',              // where it's located in the theme
        'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 0,                                   // limit the depth of the nav
        'fallback_cb' => false
    ));
} /* end bones main nav */

// the footer menu (should you choose to use one)
function skellington_footer_nav()
{

    wp_nav_menu(array(
        'container' => false,                           // remove nav container
        'menu' => 'Footer Navigation',                  // nav name
        'menu_class' => 'Footer-navigation-links',      // adding custom nav class
        'theme_location' => 'footer-nav',               // where it's located in the theme
        'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 0,                                   // limit the depth of the nav
        'fallback_cb' => false
    ));
}

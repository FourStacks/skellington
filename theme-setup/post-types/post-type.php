<?php

register_post_type(
        // Post type name
        'custom_post_type',
        // Options
        array(
            'labels' => array(
                'name' => 'Custom Types',
                'singular_name' => 'Custom Post',
                'all_items' => 'All Custom Posts',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Custom Type',
                'edit' => 'Edit',
                'edit_item' => 'Edit Post Type',
                'new_item' => 'New Post Type',
                'view_item' => 'View Post Type',
                'search_items' => 'Search Post Type',
                'not_found' =>  'Nothing found in the Database.',
                'not_found_in_trash' => 'Nothing found in Trash',
                'parent_item_colon' => ''
            ),
            'description' => 'This is the example custom post type',
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8, /* order in main side menu */
            'menu_icon' => 'dashicons', /* https://developer.wordpress.org/resource/dashicons/#controls-back */
            'rewrite'   => array( 'slug' => 'custom-type', 'with_front' => false ),
            'has_archive' => 'custom-type',
            'capability_type' => 'post',
            'hierarchical' => false,
            'show_in_rest' => true,
            'supports' => array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'trackbacks',
                'custom-fields',
                'comments',
                'revisions',
                'sticky'
            )
        )
    ); /* end of register post type */
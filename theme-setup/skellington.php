<?php
/* Welcome to Bones :)
Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

/*********************
 * LAUNCH BONES
 *********************/

// firing all initial functions at the start
add_action('after_setup_theme', 'skellington_init', 16);

function skellington_init()
{

    // launching operation cleanup
    add_action('init', 'skellington_head_cleanup');
    // remove WP version from RSS
    add_filter('the_generator', 'skellington_rss_version');
    // remove pesky injected css for recent comments widget
    add_filter('wp_head', 'skellington_remove_wp_widget_recent_comments_style', 1);
    // clean up comment styles in the head
    add_action('wp_head', 'skellington_remove_recent_comments_style', 1);
    // clean up gallery output in wp
    add_filter('gallery_style', 'skellington_gallery_style');

    // enqueue global scripts and styles
    add_action('wp_enqueue_scripts', 'skellington_scripts_and_styles', 999);
    // ie conditional wrapper

    // launching this stuff after theme setup
    skellington_theme_support();

    // adding widget areas to Wordpress (these are created in theme-setup/widget-areas.php)
    add_action('widgets_init', 'skellington_register_widget_areas');

    // cleaning up random code around images
    add_filter('the_content', 'skellington_filter_ptags_on_images');

} /* end bones ahoy */

/*********************
 * WP_HEAD GOODNESS
 * Clean up WP default head
 *********************/

function skellington_head_cleanup()
{
    // category feeds
    // remove_action( 'wp_head', 'feed_links_extra', 3 );
    // post and comment feeds
    // remove_action( 'wp_head', 'feed_links', 2 );
    // EditURI link
    remove_action('wp_head', 'rsd_link');
    // windows live writer
    remove_action('wp_head', 'wlwmanifest_link');
    // index link
    remove_action('wp_head', 'index_rel_link');
    // previous link
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    // start link
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    // links for adjacent posts
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    // WP version
    remove_action('wp_head', 'wp_generator');
    // remove emoji support
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');
    // remove WP version from css
    add_filter('style_loader_src', 'skellington_remove_wp_ver_css_js', 9999);
    // remove Wp version from scripts
    add_filter('script_loader_src', 'skellington_remove_wp_ver_css_js', 9999);


} /* end bones head cleanup */

// remove WP version from RSS
function skellington_rss_version()
{
    return '';
}

// remove WP version from scripts
function skellington_remove_wp_ver_css_js($src)
{
    if (strpos($src, 'ver='))
        $src = remove_query_arg('ver', $src);
    return $src;
}

// remove injected CSS for recent comments widget
function skellington_remove_wp_widget_recent_comments_style()
{
    if (has_filter('wp_head', 'wp_widget_recent_comments_style'))
    {
        remove_filter('wp_head', 'wp_widget_recent_comments_style');
    }
}

// remove injected CSS from recent comments widget
function skellington_remove_recent_comments_style()
{
    global $wp_widget_factory;
    if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments']))
    {
        remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
    }
}

// remove injected CSS from gallery
function skellington_gallery_style($css)
{
    return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}


/*********************
 * THEME SUPPORT
 *********************/

// Adding WP 3+ Functions & Theme Support
function skellington_theme_support()
{

    // wp thumbnails (sizes handled in functions.php)
    add_theme_support('post-thumbnails');

    // default thumb size
    set_post_thumbnail_size(125, 125, true);

    // wp custom background (thx to @bransonwerner for update)
    add_theme_support('custom-background',
        array(
            'default-image' => '',  // background image default
            'default-color' => '', // background color default (dont add the #)
            'wp-head-callback' => '_custom_background_cb',
            'admin-head-callback' => '',
            'admin-preview-callback' => ''
        )
    );

    // rss thingy
    add_theme_support('automatic-feed-links');

    // to add header image support go here: http://themble.com/support/adding-header-background-image-support/

    // adding post format support
    add_theme_support('post-formats',
        array(
            'aside',             // title less blurb
            'gallery',           // gallery of images
            'link',              // quick link to other site
            'image',             // an image
            'quote',             // a quick quote
            'status',            // a Facebook like status update
            'video',             // video
            'audio',             // audio
            'chat'               // chat transcript
        )
    );

    // wp menus
    add_theme_support('menus');

    // registering wp3+ menus
    register_nav_menus(
        array(
            'primary-nav' => 'Primary Navigation', // main nav in header
            'footer-nav' => 'Footer Navigation', // secondary nav in footer
        )
    );
}


/*********************
 * RANDOM CLEANUP ITEMS
 *********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function skellington_filter_ptags_on_images($content)
{
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
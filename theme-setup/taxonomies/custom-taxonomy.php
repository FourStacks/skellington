<?php
register_taxonomy( 'custom_category',
	// Used for which post types
	array('custom_post_type'),

	// Options
	array('hierarchical' => true,     /* tag or cateogry style */
		'labels' => array(
			'name' => 'Custom Categories',
			'singular_name' => 'Custom Category',
			'search_items' =>  'Search Custom Categories',
			'all_items' => 'All Custom Categories',
			'parent_item' => 'Parent Custom Category',
			'parent_item_colon' => 'Parent Custom Category:',
			'edit_item' => 'Edit Custom Category',
			'update_item' => 'Update Custom Category',
			'add_new_item' => 'Add New Custom Category',
			'new_item_name' => 'New Custom Category Name'
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'custom-slug' ),
	)
);
<?php

// remove WP admin bar

add_action('after_setup_theme', 'skellington_remove_admin_bar');

function skellington_remove_admin_bar() {
    show_admin_bar(false);
}
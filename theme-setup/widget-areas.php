<?php
// Sidebars & Widgetized Areas
function skellington_register_widget_areas()
{
    register_sidebar(array(
        'id' => 'widget_area_1',
        'name' => __('Widget Area 1', 'bonestheme'),
        'description' => __('The first widget area.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="Widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="Widget__title">',
        'after_title' => '</h4>',
    ));
}
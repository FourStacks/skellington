let mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');

mix.js('assets/js/app.js', 'public/js')
    .sass('assets/sass/app.scss', 'public/css')
    .browserSync('fred.test')
    .autoload({
        jquery: ['$', 'window.jQuery']
    })
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('./tailwind.js') ],
    });